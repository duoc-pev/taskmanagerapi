#!/bin/bash

export DB_DRIVER=oracle.jdbc.OracleDriver
export DB_URL=jdbc:oracle:thin:@localhost:1521:XE
export DB_USER=task_manager
export DB_PASSWORD=test1234
export DB_MAX_POOL_SIZE=10
export DB_MAX_IDLE=30

export APP_PORT=9016

cd ..
gradle shadowJar

cd api-service
#java -Xms256m -Xmx512m -Duser.timezone=UTC -Dvertx.logger-delegate-factory-class-name=io.vertx.core.logging.SLF4JLogDelegateFactory -Dlogback.configurationFile=src/main/resources/logback.xml -jar build/libs/task-manager-fat.jar

java -Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=8000 -jar build/libs/task-manager-fat.jar
