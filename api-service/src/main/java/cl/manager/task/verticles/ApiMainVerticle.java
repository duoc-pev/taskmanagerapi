package cl.manager.task.verticles;

import cl.manager.task.dao.JdbcRepositoryWrapper;
import cl.manager.task.service.business.user.UserService;
import cl.manager.task.service.business.user.UserServiceImpl;
import cl.manager.task.service.crud.user.UserServiceCRUD;
import cl.manager.task.service.crud.user.UserServiceCRUDImpl;
import cl.manager.task.util.AppEnum;
import cl.manager.task.util.SystemUtil;
import cl.manager.task.verticles.api.UserVerticle;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class ApiMainVerticle extends RestAPIVerticle {

    public static final String API_TASK_MANAGER = "/api/task-manager";
    private static final String API_HEALTHCHECK = API_TASK_MANAGER + "/healthcheck";

    // services
    private UserService userService;


    @Override
    public void start() throws Exception {
        super.start();

        // principal router
        Router router = Router.router(vertx);

        // body handler
        router.route().handler(BodyHandler.create());

        // services
        final JdbcRepositoryWrapper jdbc = JdbcRepositoryWrapper.getInstance(vertx);

        UserServiceCRUD userServiceCRUD = new UserServiceCRUDImpl(jdbc);
        this.userService = new UserServiceImpl(vertx, userServiceCRUD);


        //routes
        this.loadRoute(router);

        //deploy
        this.deployRestVerticle(router);

        //cors support
        this.enableCorsSupport(router);

        //http server
        this.createHttpServer(router, SystemUtil.getEnvironmentIntValue(AppEnum.APP_PORT.name()));
    }

    private void loadRoute(Router router) {
        //healthcheck
        router.get(API_HEALTHCHECK).handler(this::apiHealthCheck);
    }

    private void deployRestVerticle(Router router) {

        // Status API
        vertx.deployVerticle(new UserVerticle(router, this.userService));

    }

    /**
     * @param context
     */
    private void apiHealthCheck(RoutingContext context) {
        context.response().setStatusCode(200).setStatusMessage("OK").end();
    }
}
