package cl.manager.task.verticles.api;

import cl.manager.task.service.business.user.UserService;
import cl.manager.task.verticles.ApiMainVerticle;
import cl.manager.task.verticles.RestAPIVerticle;
import io.vertx.core.json.Json;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class UserVerticle extends RestAPIVerticle {

  private static final String API_USER = ApiMainVerticle.API_TASK_MANAGER.concat("/v1/user");
  private static final String API_USER_BY_ID = API_USER.concat("/:id");

  private UserService userService;

  public UserVerticle(Router router, UserService userService) {

    this.loadRoute(router);

    this.userService = userService;
  }

  private void loadRoute(Router router) {

    router.get(API_USER_BY_ID).handler(this::apiGetUserByID);
  }

  private void apiGetUserByID(RoutingContext context) {
    context.vertx().executeBlocking(future -> {
      Integer userID = Integer.parseInt(context.request().getParam("id"));

      this.userService.getUserByID(userID).setHandler(resultHandler(context, Json::encodePrettily));
    }, false, resultHandler(context, Json::encodePrettily));
  }
}
