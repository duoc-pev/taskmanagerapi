package cl.manager.task.verticles.api.util;

import cl.manager.task.model.search.CustomerSearch;
import com.google.common.base.Preconditions;

public class ValidateApiCustomerUtil extends ValidateApiUtil {

    private static final String VALIDATE_SEARCH = "Search must not be null";

    private ValidateApiCustomerUtil() {
        super(ValidateApiCustomerUtil.class.toString());
    }

    public static void search(CustomerSearch search) {
        Preconditions.checkArgument(search != null, VALIDATE_SEARCH);
        validate(search.getPagination());
    }
}
