package cl.manager.task.util;

import java.text.NumberFormat;
import java.util.Locale;

public class FormatUtil {

    public static String getFormattedRut(Integer prefix, String dv) {
        return NumberFormat.getIntegerInstance(Locale.GERMAN).format(prefix) + "-" + dv;
    }

    public static String getFormattedPhoneNumber(Integer countryCode, Integer areaCode, Integer number) {
        if (null != countryCode && null != number) {
            String numberString = number.toString();
            return "(+" + countryCode + " " + areaCode + ") " + numberString.substring(0, 4) + "-" + numberString.substring(4, 8);
        } else {
            return null;
        }
    }
}
