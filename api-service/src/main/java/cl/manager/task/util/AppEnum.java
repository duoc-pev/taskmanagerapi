package cl.manager.task.util;

public enum AppEnum {
    APP_PORT,
    APP_API_URL_POST_AUTHORIZATION,
    APP_API_URL_POST_USER_LOGIN,
    APP_API_TO_API_USER_LOGIN,
    APP_API_TO_API_PWD_LOGIN
}