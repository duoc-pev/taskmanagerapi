package cl.manager.task.util;

public class MessageUtil {
    private MessageUtil() {
        throw new IllegalAccessError(InstantUtil.class.toString());
    }

    public static final String ERROR_CUSTOMER = "No customer exists with that ID";

    public static final String ERROR_ADDRESS = "No address exists with that ID";

    public static final String ERROR_GENDER = "No gender exists with that ID";

    public static final String ERROR_PHONE = "No phone exists with that ID";

    public static final String ERROR_EMPLOYEE = "No employee exists with that ID";

    public static final String ERROR_ROLE = "No role exists with that ID";

    public static final String ERROR_GROUP = "No group exists with that ID";

    public static final String ERROR_BRANCH = "No branch office exists with that ID";

    public static final String ERROR_CATEGORY = "No product category exists with that ID";

    public static final String ERROR_UNIT = "No product unit exists with that ID";

    public static final String ERROR_SUPPLIER = "No supplier exists with that ID";
}
