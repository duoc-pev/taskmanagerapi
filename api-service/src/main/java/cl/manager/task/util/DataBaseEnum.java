package cl.manager.task.util;

public enum DataBaseEnum {

    DB_URL, DB_DRIVER, DB_USER, DB_PASSWORD, DB_MAX_POOL_SIZE, DB_MAX_IDLE
}

