package cl.manager.task.dao.query;

import cl.manager.task.model.commons.address.City;
import cl.manager.task.model.commons.address.Commune;
import cl.manager.task.model.commons.address.District;
import cl.manager.task.model.search.AddressSearch;
import cl.manager.task.model.search.PhoneSearch;

import static cl.manager.task.util.Constant.ADD;
import static cl.manager.task.util.Constant.AND;
import static cl.manager.task.util.Constant.FROM;
import static cl.manager.task.util.Constant.LEFT_OUTER_JOIN;
import static cl.manager.task.util.Constant.SELECT;
import static cl.manager.task.util.Constant.WHERE;

public class QueryCommonsUtil {

    private QueryCommonsUtil() {
        throw new IllegalAccessError(QueryCommonsUtil.class.toString());
    }

    public static String getAddressByIdSql() {
        StringBuilder where = new StringBuilder();

        where.append(WHERE);
        where.append(" ad.add_id = ? ");

        return selectAddress().append(where).toString();
    }

    public static String searchAddressSql(AddressSearch search) {
        StringBuilder where = new StringBuilder();
        where.append(WHERE);
        where.append(" ad.add_id > 0 ");
        where.append(searchWhereAddress(search).toString());

        if (null != search.getAddress().getCommune()) {
            where.append(searchWhereCommune(search.getAddress().getCommune()));
        }

        if (null != search.getAddress().getCity()) {
            where.append(searchWhereCity(search.getAddress().getCity()));
        }

        if (null != search.getAddress().getDistrict()) {
            where.append(searchWhereDistrict(search.getAddress().getDistrict()));
        }
        
        return selectAddress().append(where).toString();
    }

    public static String getDistrictListSql() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectDistrict());

        return query.toString();
    }

    public static String getCityByDistrictIdSql() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectCity());
        query.append(WHERE);
        query.append(" ci.city_district_id = ? ");

        return query.toString();
    }

    public static String getCommuneByCityIdSql() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectCommune());
        query.append(WHERE);
        query.append(" com.com_city_id = ? ");

        return query.toString();
    }

    public static String getGenderListSql() {

        return selectGender().toString();
    }

    public static String getGenderByIdSql() {
        StringBuilder where = new StringBuilder();

        where.append(WHERE);
        where.append(" ge.gender_id = ? ");

        return selectGender().append(where).toString();
    }

    public static String getPhoneByIdSql() {
        StringBuilder where = new StringBuilder();

        where.append(WHERE);
        where.append(" ph.phone_id = ? ");

        return selectPhone().append(where).toString();
    }

    public static String searchPhoneSql(PhoneSearch search) {
        StringBuilder where = new StringBuilder();

        where.append(WHERE);
        where.append(" ph.phone_id > 0 ");
        where.append(searchPhoneWhere(search)).toString();

        return selectPhone().append(where).toString();
    }

    public static String saveAddressSql() {
        StringBuilder query = new StringBuilder();

        query.append("begin sp_create_address_full ");
        query.append(" (?, ?, ?, ?, ?); ");
        query.append(" end;");

        return query.toString();
    }

    public static String savePhoneSql() {
        StringBuilder query = new StringBuilder();

        query.append("begin sp_create_phone ");
        query.append(" (?, ?, ?, ?, ?); ");
        query.append(" end;");

        return query.toString();
    }

    private static StringBuilder selectAddress() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectAddressOnly());
        query.append(ADD);
        query.append(selectDistrictOnly());
        query.append(ADD);
        query.append(selectCitOnly());
        query.append(ADD);
        query.append(selectCommuneOnly());

        query.append(fromWhereAddress());

        return query;
    }

    static StringBuilder getFullAddressQuery() {
        StringBuilder query = new StringBuilder();

        query.append(selectAddressOnly());
        query.append(ADD);
        query.append(selectDistrictOnly());
        query.append(ADD);
        query.append(selectCitOnly());
        query.append(ADD);
        query.append(selectCommuneOnly());

        return query;
    }

    private static StringBuilder selectDistrict() {
        StringBuilder query = new StringBuilder();

        query.append(selectDistrictOnly());
        query.append(FROM);
        query.append(" district dist");

        return query;
    }

    private static StringBuilder selectCity() {
        StringBuilder query = new StringBuilder();

        query.append(selectCitOnly());
        query.append(FROM);
        query.append(" city ci");

        return query;
    }

    private static StringBuilder selectCommune() {
        StringBuilder query = new StringBuilder();

        query.append(selectCommuneOnly());
        query.append(FROM);
        query.append(" commune com");

        return query;
    }

    private static String selectAddressOnly() {
        StringBuilder query = new StringBuilder();

        query.append(" ad.add_id as address_id, ");
        query.append(" ad.add_line_1 as line_1_address, ");
        query.append(" ad.add_line_2 as line_2_address, ");
        query.append(" ad.add_postal_code as postal_code ");

        return query.toString();
    }

    private static String selectDistrictOnly() {
        StringBuilder query = new StringBuilder();

        query.append(" dist.district_id, ");
        query.append(" dist.district_name, ");
        query.append(" dist.district_order, ");
        query.append(" dist.district_roman ");

        return query.toString();
    }

    private static String selectCitOnly() {
        StringBuilder query = new StringBuilder();

        query.append(" ci.city_id, ");
        query.append(" ci.city_name ");

        return query.toString();
    }

    private static String selectCommuneOnly() {
        StringBuilder query = new StringBuilder();

        query.append(" com.com_id, ");
        query.append(" com.com_name ");

        return query.toString();
    }

    private static StringBuilder fromWhereAddress() {
        StringBuilder query = new StringBuilder();

        query.append(FROM);
        query.append(" addresses ad ")
                .append(LEFT_OUTER_JOIN)
                .append(" commune com on (ad.add_com_id = com.com_id) ")
                .append(LEFT_OUTER_JOIN)
                .append(" city ci on (com.com_city_id = ci.city_id) ")
                .append(LEFT_OUTER_JOIN)
                .append(" district dist on (ci.city_district_id = dist.district_id) ");

        return query;

    }

    private static StringBuilder searchWhereAddress(AddressSearch search) {
        StringBuilder query = new StringBuilder();

        query.append(" ");
        if (null != search.getAddress().getId()) {
            query.append(AND);
            query.append(" ad.add_id = ").append(search.getAddress().getId());
        }

        if (null != search.getAddress().getLine1()) {
            query.append(AND);
            query.append(" ad.add_line_1 LIKE '").append(search.getAddress().getLine1()).append("%' ");
        }

        if (null != search.getAddress().getLine2()) {
            query.append(AND);
            query.append(" ad.add_line_2 LIKE '").append(search.getAddress().getLine1()).append("%' ");
        }

        if (null != search.getAddress().getPostalCode()) {
            query.append(AND);
            query.append(" ad.add_postal_code LIKE '").append(search.getAddress().getPostalCode()).append("%' ");
        }

        return query;
    }

    private static StringBuilder searchWhereCommune(Commune search) {
        StringBuilder query = new StringBuilder();

        query.append(" ");
        if (null != search.getCommuneId()) {
            query.append(AND);
            query.append(" com.com_id = ").append(search.getCommuneId());
        }

        if (null != search.getCommuneName()) {
            query.append(AND);
            query.append(" com.com_name LIKE '").append(search.getCommuneName()).append("%' ");
        }

        return query;
    }

    private static StringBuilder searchWhereCity(City search) {
        StringBuilder query = new StringBuilder();

        query.append(" ");
        if (null != search.getCityId()) {
            query.append(AND);
            query.append(" ci.city_id = ").append(search.getCityId());
        }

        if (null != search.getCityName()) {
            query.append(AND);
            query.append(" ci.city_name LIKE '").append(search.getCityName()).append("%' ");
        }

        return query;
    }

    private static StringBuilder searchWhereDistrict(District search) {
        StringBuilder query = new StringBuilder();

        query.append(" ");
        if (null != search.getId()) {
            query.append(AND);
            query.append(" dist.district_id = ").append(search.getId());
        }

        if (null != search.getLabel()) {
            query.append(AND);
            query.append(" dist.district_name LIKE '").append(search.getLabel()).append("%' ");
        }

        if (null != search.getRoman()) {
            query.append(AND);
            query.append(" dist.district_roman LIKE '").append(search.getRoman()).append("%' ");
        }

        return query;
    }

    private static StringBuilder selectGender() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(" ge.gender_id,");
        query.append(" ge.gender_name ");
        query.append(FROM);
        query.append(" gender ge ");

        return query;
    }

    private static StringBuilder selectPhone() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(" ph.phone_id, ");
        query.append(" ph.phone_country_code, ");
        query.append(" ph.phone_area_code, ");
        query.append(" ph.phone_number, ");
        query.append(" ph.phone_whatsapp ");
        query.append(FROM);
        query.append(" phone ph ");

        return query;
    }

    static StringBuilder selectPhoneOnly() {
        StringBuilder query = new StringBuilder();

        query.append(" ph.phone_id, ");
        query.append(" ph.phone_country_code, ");
        query.append(" ph.phone_area_code, ");
        query.append(" ph.phone_number, ");
        query.append(" ph.phone_whatsapp ");

        return query;
    }

    private static StringBuilder searchPhoneWhere(PhoneSearch search) {
        StringBuilder query = new StringBuilder();

        query.append(" ");
        if (null != search.getPhone().getPhoneId()) {
            query.append(AND);
            query.append(" ph.phone_id = ").append(search.getPhone().getPhoneId());
        }

        if (null != search.getPhone().getCountryCode()) {
            query.append(AND);
            query.append(" ph.phone_country_code = ").append(search.getPhone().getCountryCode());
        }

        if (null != search.getPhone().getAreaCode()) {
            query.append(AND);
            query.append(" ph.phone_area_code = ").append(search.getPhone().getAreaCode());
        }

        if (null != search.getPhone().getNumber()) {
            query.append(AND);
            query.append(" ph.phone_number LIKE '").append(search.getPhone().getNumber()).append("%' ");
        }

        if (null != search.getPhone().getHasWhatsapp()) {
            query.append(AND);
            query.append(" ph.phone_whatsapp = '").append(search.getPhone().getHasWhatsapp() ? 'Y' : 'N').append("' ");
        }

        return query;
    }

}
