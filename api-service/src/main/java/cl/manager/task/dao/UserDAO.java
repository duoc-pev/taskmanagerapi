package cl.manager.task.dao;

public class UserDAO {

  private static UserDAO instance;
  private final JdbcRepositoryWrapper jdbc;

  private UserDAO (JdbcRepositoryWrapper jdbc) {
    this.jdbc = jdbc;
  }

  public static synchronized UserDAO getInstance(final JdbcRepositoryWrapper jdbc) {
    if (null == instance) {
      instance = new UserDAO(jdbc);
    }

    return instance;
  }

}
