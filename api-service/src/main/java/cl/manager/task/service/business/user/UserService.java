package cl.manager.task.service.business.user;

import cl.manager.task.model.user.User;
import io.vertx.core.Future;

public interface UserService {

  Future<User> getUserByID(Integer userID);
}
