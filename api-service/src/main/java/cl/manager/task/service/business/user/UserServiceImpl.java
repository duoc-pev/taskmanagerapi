package cl.manager.task.service.business.user;

import cl.manager.task.model.user.User;
import cl.manager.task.service.crud.user.UserServiceCRUD;
import io.vertx.core.Future;
import io.vertx.core.Vertx;

public class UserServiceImpl implements UserService {

  UserServiceCRUD userServiceCRUD;

  public UserServiceImpl(Vertx vertx, UserServiceCRUD userServiceCRUD) {
    this.userServiceCRUD = userServiceCRUD;
  }

  @Override
  public Future<User> getUserByID(Integer userID) {
    return this.userServiceCRUD.getUserByID(userID);
  }
}
