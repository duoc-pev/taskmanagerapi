package cl.manager.task.service.crud.user;

import cl.manager.task.dao.JdbcRepositoryWrapper;
import cl.manager.task.dao.UserDAO;
import cl.manager.task.model.search.SearchUser;
import cl.manager.task.model.user.Permission;
import cl.manager.task.model.user.PermissionAssign;
import cl.manager.task.model.user.Role;
import cl.manager.task.model.user.User;
import io.vertx.core.Future;

import java.util.List;

public class UserServiceCRUDImpl implements UserServiceCRUD {

  private final UserDAO userDAO;

  public UserServiceCRUDImpl(JdbcRepositoryWrapper jdbc) {
    this.userDAO = UserDAO.getInstance(jdbc);
  }

  /* User Methods */

  @Override
  public Future<List<User>> getUserListBySearch(SearchUser search) {
    return null;
  }

  @Override
  public Future<User> getUserByID(Integer userID) {
    return null;
  }

  @Override
  public Future<Void> createUser(User user) {
    return null;
  }

  @Override
  public Future<Void> updateUser(User user) {
    return null;
  }


  /* Role Methods */

  @Override
  public Future<List<Role>> getRoles() {
    return null;
  }

  @Override
  public Future<Void> createRole(Role role) {
    return null;
  }

  @Override
  public Future<Void> updateRole(Role role) {
    return null;
  }


  /* Permission Methods */

  @Override
  public Future<List<Permission>> getPermissionListByRoleID(Integer roleID) {
    return null;
  }

  @Override
  public Future<Void> createPermission(Permission permission) {
    return null;
  }

  @Override
  public Future<Void> updatePermission(Permission permission) {
    return null;
  }

  @Override
  public Future<Void> assignPermissionToRole(PermissionAssign permissionAssign) {
    return null;
  }
}
