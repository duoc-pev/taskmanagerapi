package cl.manager.task.service.crud.user;

import cl.manager.task.model.search.SearchUser;
import cl.manager.task.model.user.PermissionAssign;
import cl.manager.task.model.user.Permission;
import cl.manager.task.model.user.Role;
import cl.manager.task.model.user.User;
import io.vertx.core.Future;

import java.util.List;

public interface UserServiceCRUD {

  /* User Methods */

  Future<List<User>> getUserListBySearch(SearchUser search);

  Future<User> getUserByID(Integer userID);

  Future<Void> createUser(User user);

  Future<Void> updateUser(User user);


  /* Role Methods */

  Future<List<Role>> getRoles();

  Future<Void> createRole(Role role);

  Future<Void> updateRole(Role role);


  /* Permission Methods */

  Future<List<Permission>> getPermissionListByRoleID(Integer roleID);

  Future<Void> createPermission(Permission permission);

  Future<Void> updatePermission(Permission permission);

  Future<Void> assignPermissionToRole(PermissionAssign permissionAssign);

}
