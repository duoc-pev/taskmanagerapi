package cl.manager.task.model.user;

import lombok.Data;

@Data
public class PermissionAssign {

  private Integer roleID;
  private Integer permissionID;
}
