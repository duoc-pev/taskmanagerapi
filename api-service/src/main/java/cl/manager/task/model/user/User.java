package cl.manager.task.model.user;

import lombok.Data;

import java.util.Date;

@Data
public class User {

  private Integer id;
  private Role role;
  private Integer rut;
  private char dv;
  private String firstName;
  private String secondName;
  private Date birthDate;
  private Gender gender;
  private Date admissionDate;
  private Date resignationDate;
  private Date updateDate;

}
