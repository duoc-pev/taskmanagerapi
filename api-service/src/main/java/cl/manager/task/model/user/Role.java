package cl.manager.task.model.user;

import lombok.Data;

import java.util.List;

@Data
public class Role {

  private Integer id;
  private String name;
  private String description;
  private List<Permission> permissionList;

}
