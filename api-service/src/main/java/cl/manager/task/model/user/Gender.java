package cl.manager.task.model.user;

import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Gender {
    private Integer id;
    private String label;

    public Gender(Integer genderId) {
        this.id = genderId;
    }

    public Gender(JsonObject json) {
        this.id = json.getInteger("GENDER_ID");
        this.label = json.getString("GENDER_NAME");
    }
}
